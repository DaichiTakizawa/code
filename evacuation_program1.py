#! /usr/bin/python
# *-* encoding: utf-8 *-*

from gurobipy import *

capF = [10,5]
capS = [10,5]
pF = [50,50]
pS = [50,50]
T = 13
N = 2

model = Model("ev1")

rF = {}
RF = {}
rS = {}
RS = {}
RA = {}
for n in range(N):
    for t in range(T):
        rF[n,t] = model.addVar(vtype="C", name='rF[%d,%d]' % (n,t))       
        RF[n,t] = model.addVar(vtype="C", name='RF[%d,%d]' % (n,t))
        rS[n,t] = model.addVar(vtype="C", name='rS[%d,%d]' % (n,t))       
        RS[n,t] = model.addVar(vtype="C", name='RS[%d,%d]' % (n,t))
        RA[t] = model.addVar(vtype="C", name='RA[%d]' % (t))
model.update()

for n in range(N):
    L = n+1
    model.addConstr(quicksum(rF[n,t] for t in range(T)) <= pF[n])
    model.addConstr(quicksum(rS[n,t] for t in range(T)) <= pS[n])
    for i in range(T):
        model.addConstr(quicksum(rF[n,t] for t in range(i)) == RF[n,i])
        if i-L > 0:
            model.addConstr(quicksum(rS[n,t] for t in range(i-L)) == RS[n,i])
        else:
           RS[n,i] = 0
        
for t in range(T):
    rF[1,t].ub = capF[1]
    rS[1,t].ub = capS[1]
    model.addConstr(rF[1,t] <= capF[0] - rF[0,t])
    model.addConstr(quicksum(RF[n,t] + RS[n,t] for n in range(N)) == RA[t])
    if t < 1:
        model.addConstr(rS[0,t] <= capS[0])
    else:
        model.addConstr(rS[0,t] <= capS[0] - rS[1,t-1])
    
model.setObjective(quicksum(RA[t] for t in range(T)),GRB.MAXIMIZE)
model.optimize()

print "optimal value:\t%8.4f" % model.ObjVal
for n in range(N):
    for t in range(T):
        print "%s:\t%8.4f" % (rF[n,t].VarName, rF[n,t].X)
for n in range(N):
    for t in range(T):
        print "%s:\t%8.4f" % (rS[n,t].VarName, rS[n,t].X)
for t in range(T):
    print "%s:\t%8.4f" % (RA[t].VarName, RA[t].X)
        
f = open("evacuation_result1.txt","w")
f.write("optimal value:\t%8.4f \n" % model.ObjVal)
for n in range(N):
    for t in range(T):
        f.write("%s:\t%8.4f \n" % (rF[n,t].VarName, rF[n,t].X))
for n in range(N):
    for t in range(T):
        f.write("%s:\t%8.4f \n" % (rS[n,t].VarName, rS[n,t].X))
for t in range(T):
    f.write("%s:\t%8.4f \n" % (RA[t].VarName, RA[t].X))
f.close()

