#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib
import pylab

x = []
y = []

pylab.plot(x,y, color="blue", label="") 
pylab.plot([x[], x[]],[0, y[]], color="blue", linestyle="--")

pylab.fill_between(x, 0, y, color='green', alpha=0.25)

pylab.xlim(,)
pylab.ylim(,)

pylab.xlabel('')
pylab.ylabel('')

pylab.legend(loc='upper left')

pylab.savefig('result_graph.png')

pylab.show()
