#!/usr/bin/env python
# -*- coding: utf-8 -*-

from matplotlib import pylab
from mpl_toolkits.mplot3d import Axes3D
import numpy

x = []
y = []
z = [] 

fig = pylab.figure()
ax = Axes3D(fig)

ax.set_xlim(0, max(x)*1.1)
ax.set_ylim(0, max(y)*1.1)
ax.set_zlim(0, max(z)*1.01)

ax.set_xlabel("")
ax.set_ylabel("")
ax.set_zlabel("")

ax.scatter3D(x, y, z, color=(1.0, 0.0, 0.0), marker="o", s=20)

pylab.savefig("graph2.png")

pylab.show()
