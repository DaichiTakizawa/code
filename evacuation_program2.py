#! /usr/bin/python
# *-* encoding: utf-8 *-*

from gurobipy import *

cap = [20,10]
pF = [50,50]
pS = [50,50]
T = 13
N = 2

capF = [10,0]
capS = [10,0]

f = open("evacuation_result2.txt","w")
for n in range(N):
    f.write("pF[%d]:\t%d \n" % (n, pF[n]))
for n in range(N):
    f.write("pS[%d]:\t%d \n" % (n, pS[n]))
f.close()

model = Model("ev2")

for a in range(1,10):
    capF[1] = a 
    capS[1] = cap[1] - a

    if capF[1] >= capF[0]:
        capF[1] = capF[0] - 1
        capS[1] = cap[1] - capF[1]
    if capS[1] >= capS[0]:
        capS[1] = capS[0] - 1
        capF[1] = cap[1] - capS[1]
    
    rF = {}
    RF = {}
    rS = {}
    RS = {}
    RA = {}
    for n in range(N):
        for t in range(T):
            rF[n,t] = model.addVar(vtype="C", name='rF[%d,%d]' % (n,t))
            RF[n,t] = model.addVar(vtype="C", name='RF[%d,%d]' % (n,t))
            rS[n,t] = model.addVar(vtype="C", name='rS[%d,%d]' % (n,t))    
            RS[n,t] = model.addVar(vtype="C", name='RS[%d,%d]' % (n,t))
            RA[t] = model.addVar(vtype="C", name='RA[%d]' % (t))
    model.update()

    for n in range(N):
        L = n+1
        model.addConstr(quicksum(rF[n,t] for t in range(T)) <= pF[n])
        model.addConstr(quicksum(rS[n,t] for t in range(T)) <= pS[n])
        for i in range(T):
            model.addConstr(quicksum(rF[n,t] for t in range(i)) == RF[n,i])
            if i-L > 0:
                model.addConstr(quicksum(rS[n,t] for t in range(i-L)) == RS[n,i])
            else:
                RS[n,i] = 0
        
    for t in range(T):
        rF[1,t].ub = capF[1]
        rS[1,t].ub = capS[1]
        model.addConstr(rF[1,t] <= capF[0] - rF[0,t])
        model.addConstr(quicksum(RF[n,t] + RS[n,t] for n in range(N)) == RA[t])
        if t < 1:
            model.addConstr(rS[0,t] <= capS[0])
            rF[1,t] == capF[1]
            rS[1,t] == capS[1]
        else:
            model.addConstr(rS[0,t] <= capS[0] - rS[1,t-1])
            model.addConstr(rF[1,t] <= rF[1,t-1])
            model.addConstr(rS[1,t] <= rS[1,t-1])

    model.setObjective(quicksum(RA[t] for t in range(T)),GRB.MAXIMIZE)
    model.optimize()

    print "optimal value:\t%d" % model.ObjVal
    for t in range(T):
        print "%s:\t%d" % (RA[t].VarName, RA[t].X)
        
    f = open("evacuation_result2.txt","a")
    for n in range(N):
        f.write("capF[%d]:\t%d \n" % (n, capF[n]))
    for n in range(N):
        f.write("capS[%d]:\t%d \n" % (n, capS[n]))
    f.write("optimal value:\t%d \n" % model.ObjVal)
    for t in range(T):
        if t == T-1:
            f.write("%s:\t%d \n\n" % (RA[t].VarName, RA[t].X))
        else:
            f.write("%s:\t%d \n" % (RA[t].VarName, RA[t].X))
    f.close()

